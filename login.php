<?php

require_once 'dbconnect.inc.php';

session_start();

$emailErr = "";
$passErr = "";
if($_SERVER["REQUEST_METHOD"] == "POST") {
if(empty($_POST["uname"])) {
                $emailErr = "Name is required";
                } else {
                $username = mysqli_real_escape_string($dbLink, $_POST["uname"]);
                if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
                                $emailErr = "Invalid email format";
                                }
                }
if(empty($_POST["psw"])) {
                $passErr = "Password is required";
} else {
  $enteredPassword = mysqli_real_escape_string($dbLink, $_POST["psw"]);
}

$sqlAuth =  "SELECT email, password FROM users WHERE email = '$username'";

$resultAuth = mysqli_query($dbLink, $sqlAuth);

$authValue = mysqli_fetch_assoc($resultAuth);

$storedPasswordHash = $authValue['password'];

if(isset($enteredPassword) && password_verify($enteredPassword, $storedPasswordHash)) {
    echo 'Login success';
    header("Location: index.php");
} else {
    echo 'Login failed';
}

}
?>
<!DOCTYPE html>
<html>
<body>
 
<h2>Login Form</h2>
 
<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
 
  <div class="container">
    <label><b>Username*</b></label>
    <input type="text" placeholder="Enter Username" name="uname">
                <span class="error">* <?php echo $emailErr;?></span>
                <br><br>
 
    <label><b>Password*</b></label>
    <input type="password" placeholder="Enter Password" name="psw">
    <span class="error">* <?php echo $passErr;?></span>
                <br><br>
       
    <button type="submit">Login</button>
  </div>
</form>
 
</body>
</html>