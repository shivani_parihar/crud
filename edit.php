<?php
// Set up db connection
require_once 'dbconnect.inc.php';

session_start();

$id = $_GET['id'];
//echo $id;

$idSql = mysqli_real_escape_string($dbLink, $id);
$sql = "SELECT * FROM movies WHERE id='$idSql'";
$result = mysqli_query($dbLink, $sql);


$movie = mysqli_fetch_assoc($result);

if(is_null($movie)) {
	header('Location: error.php');
}

if (! is_numeric($id)) {
	$msg = "Invalid ID given";
	$_SESSION['$msg'] = $msg;
	header("Location: index.php");
	exit();
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Movie Release Information</title>
</head>
<body>
<h1>Edit Movie</h1>
<form action="editDo.php" method="post">
	<p>
	<label for="movie_title">Movie Title:</label>
	<input type="text" id="movie_title" name="movie_title" value="<?php echo htmlspecialchars($movie['movie_title'])?>" />
	<input type="hidden" id="id" name="id" value="<?php echo htmlspecialchars($movie['id'])?>" />
	</p>

	<p>
		<label for="synopsis">Synopsis:</label>

	</p>
	<textarea name="synopsis" id="synopsis" rows="5" cols="40"><?php echo htmlspecialchars($movie['synopsis'])?></textarea>
	<p>
		<label for="release_date">Release Date:</label>
		<input type="text" id ="release_date" name="release_date" value="<?php echo htmlspecialchars($movie['release_date'])?>" />
	</p>
	

	<p>
		<label for="rating">Rating: </label>
			<select type="text" name="rating" id="rating" value="<?php echo htmlspecialchars($movie['rating'])?>">
				<option value="1" <?php echo $movie['rating'] == 1 ? 'selected="selected"' : ''?>>1 -Very Poor</option>
				<option value="2" <?php echo $movie['rating'] == 2 ? 'selected="selected"' : ''?>>2 -Poor</option>
				<option value="3" <?php echo $movie['rating'] == 3 ? 'selected="selected"' : ''?>>3 -OK</option>
				<option value="4" <?php echo $movie['rating'] == 4 ? 'selected="selected"' : ''?>>4 -Good</option>
				<option value="5" <?php echo $movie['rating'] == 5 ? 'selected="selected"' : ''?>>5 -Great!</option>
			</select>
	</p>
	<input type="submit" name="submit" value="Edit Movie" />
</form>
</body>
</html>