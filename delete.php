<?php
// Set up db connection
require_once 'dbconnect.inc.php';

session_start();

$id = $_GET['id'];
//echo $id;
if (! is_numeric($id)) {
	$msg = "Invalid ID given";
	$_SESSION['$msg'] = $msg;
	header("Location: index.php");
	exit();
}

$idSql = mysqli_real_escape_string($dbLink, $id);

$sql = "SELECT * FROM movies WHERE id='$idSql'";
$result = mysqli_query($dbLink, $sql);

$movie = mysqli_fetch_assoc($result);

?>

<!DOCTYPE html>
<head>
	<title>Movie Release Information</title>
</head>
<body>
	<h1>Delete Movie</h1>
	<form action="deleteDo.php" method="post" onsubmit="return confirm('Are you sure you want to delete');">
	<label for="movie_title">Movie Title:</label>
	<input type="text" id="movie_title" name="movie_title" value="<?php echo htmlspecialchars($movie['movie_title'])?>" />
	<input type="hidden" id="id" name="id" value="<?php echo htmlspecialchars($movie['id'])?>" />
	
	<input type="submit" name="submit" value="Delete Movie" />
</body>