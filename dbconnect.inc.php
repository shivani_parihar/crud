<?php
$dbHost = "localhost";
$dbUser = "root";
$dbPassword = "";
$dbName = "movies";

// Attempt to connect to DB server, error on failure
$dbLink = mysqli_connect($dbHost, $dbUser, $dbPassword, $dbName);
if(!$dbLink) {
	die("could not connect to Database". mysqli_connect_errno());
}

//Attempt to select the Database, error on failure
//$dbSelectOk = mysqli_select_db($dbLink, $dbName);
if (!mysqli_select_db($dbLink, $dbName)) {
	die('Can\'t use $dbName : '. mysqli_error());
}
?>