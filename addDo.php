<?php
// Set up db connection
require_once 'dbconnect.inc.php';

session_start();

//prepare variable
$movie_title = mysqli_real_escape_string($dbLink, $_POST['movie_title']);
$synopsis = mysqli_real_escape_string($dbLink, $_POST['synopsis']);
$release_date = mysqli_real_escape_string($dbLink, $_POST['release_date']);
$rating = mysqli_real_escape_string($dbLink, $_POST['rating']);

echo $movie_title;
echo $synopsis;
echo $release_date;
echo $rating;

//Build SQL Statement
$sql = "INSERT INTO movies (movie_title, synopsis, release_date, rating)
VALUES ('$movie_title', '$synopsis', '$release_date', '$rating')";

//$sql = "INSERT INTO movies (id, movie_title, synopsis, release_date, rating)
//VALUES (1, 'Circle', 'Social Media', '12/12/2017', '3.0')";

//Run the sql statement, INSERT statements return true or  false
if (mysqli_query($dbLink, $sql)) {
	$id = mysqli_insert_id();
	//prep message
	$msg = "Record # $id added";
} else {
	printf("Record failed");
	$msg = "Error creating record. MySQL Eror: " . mysqli_error($dbLink);
}

$_SESSION['msg'] = $msg;
header("Location: index.php");
exit();
?>