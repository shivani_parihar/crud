<!DOCTYPE html>
<html>
<head>
	<title>Movie Release Information</title>

	<style>
		.container{
			margin: 20px;
		}
	</style>
</head>
<body>
	<h1>Add a New Movie</h1>
	<div class="container">
	<form action="addDo.php" method="post">
		<p>
			<label for="movie_title">Movie Title:</label>
			<input type="text" id="movie_title", name="movie_title" value="" maxlength="80" size="40" />
		</p>

		<p>
			<label for="synopsis">Synopsis:</label>
			<textarea name="synopsis" id="synopsis" rows="5" cols="40" ></textarea>
		</p>

		<p>
			<label for="release_date">Release Date:</label>
			<input type="text" id="release_date" name="release_date" value="" maxlength="20" size="20"/>
		</p>

		<p>
			<label for="rating">Rating: </label>
			<select name="rating" id="rating">
				<option value="1">1 -Very Poor</option>
				<option value="2">2 -Poor</option>
				<option value="3">3 -OK</option>
				<option value="4">4 -Good</option>
				<option value="5">5 -Great!</option>
			</select>
		</p>

		<input type="submit" name="submit" value="Add Movie" />
	</form>
	</div>
</body>