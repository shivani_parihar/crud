<?php
$dbHost = "localhost";
$dbUser = "root";
$dbPassword = "";
$dbName = "movies";

include 'nav.php';

// Attempt to connect to DB server, error on failure
$dbLink = mysqli_connect($dbHost, $dbUser, $dbPassword, $dbName);
if(!$dbLink) {
	die("could not connect to Database". mysqli_connect_errno());
}

//Attempt to select the Database, error on failure
//$dbSelectOk = mysqli_select_db($dbLink, $dbName);
if (!mysqli_select_db($dbLink, $dbName)) {
	die('Can\'t use $dbName : '. mysqli_error());
}

// Prepare and run the SQL statement

$sql = "SELECT * FROM movies";
$result = mysqli_query($dbLink, $sql);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Movie Release Information</title>
	<style type="text/css">
		.container {
			margin: 20px;
		}
		.heading {
			margin: 20px;
		}
	</style>
</head>
<body>
	<h1 class="heading">Movie Release Information</h1>
	<div class="container">
	<table>
		<tr>
			<th>ID</th>
			<th>Movie Title</th>
			<th>Synopsis</th>
			<th>Release Date</th>
			<th>Rating</th>
			<th colspan='2'>Actions</th>
		</tr>

		<?php
		while ($row = mysqli_fetch_assoc($result)) {
			print "<tbody>";
			print "<tr>";
			print "<td>" . $row['id']. "</td>";
			print "<td>" . htmlspecialchars($row['movie_title']) . "</td>";
			print "<td>" . htmlspecialchars($row['synopsis']) . "</td>";
			print "<td>" . htmlspecialchars($row['release_date']) . "</td>";
			print "<td>" . htmlspecialchars($row['rating']) . "</td>";
			print "<td><a href='edit.php?id=" . $row['id'] . "'>Edit</a></td>";
			print "<td><a href='delete.php?id=" . $row['id'] . "'>Delete</a></td>";
			print "</tr>";
			print "</tbody>";
		}
?>
	</table>
	<div>
</body>
</html>

<style>
table, th, td {
	border: 1px solid black;
}
</style>